slave_devices/
===================================

.. toctree::
    :maxdepth: 1

    modbus_widgets.rst
    slave_devices_grid.rst
    slave_devices_model.rst
    slave_dialog.rst

