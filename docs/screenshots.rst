Screenshots
===================


.. image:: screenshots/openplc_desktop.2.png

.. image:: screenshots/servers.png

.. image:: screenshots/interactive_server.png

.. image:: screenshots/ssh_term.png

.. image:: screenshots/ini_editor.png

.. image:: screenshots/slaves.png

.. image:: screenshots/settings.png

.. image:: screenshots/mbconfig.png