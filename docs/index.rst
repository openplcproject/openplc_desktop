
OpenPLC Desktop
=========================================

This is a desktop app (pyqthon3/PyQt5) to run alongside the new
`openplc-server <https://openplcproject.gitlab.io/openplc_go/>`_ and REST api written in go

.. note:: This project is still at alpha stage

.. image:: screenshots/openplc_desktop.1.png
  :height: 400px

Pre-Requisites
------------------------

For linux the following packages are required (note the crypto stuff can take a while to compile):

.. code-block:: console

    sudo apt-get install libssl-dev libffi-dev python3-pyqt5

For windows:

.. code-block:: console

   pip3 install PyQt5


Install from pypi
---------------------

From PyPi https://pypi.org/project/openplc-desktop/

.. code-block:: console

    pip3 install openplc-desktop
    openplc-desktop.py

.. note::

    Some deb packages may need to be installed







From Git
------------

.. code-block:: console

    # clone with http
    git clone https://gitlab.com/openplcproject/openplc_desktop.git

    # or git
    git clone git@gitlab.com:openplcproject/openplc_desktop.git

    cd openplc_desktop
    ./openplc-desktop.py



.. toctree::
   :maxdepth: 2

   screenshots.rst
   api/index.rst
   todo








   
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

