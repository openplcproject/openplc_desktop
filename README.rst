
OpenPLC Desktop
=========================================

This is a desktop app (pyqthon3/PyQt5) to run alongside the
new `openplc-server <https://openplcproject.gitlab.io/openplc_go/>`_ and REST api written in go

- `Project Home <https://openplcproject.gitlab.io/openplc_desktop/>`_

.. note:: This project is still at alpha stage

Install Package
---------------------

From PyPi https://pypi.org/project/openplc-desktop/

.. code-block:: console

    pip3 install openplc-desktop
    openplc-desktop.py



Install from git
---------------------

.. code-block:: console

    # clone with http
    git clone https://gitlab.com/openplcproject/openplc_desktop.git

    # or git
    git clone git@gitlab.com:openplcproject/openplc_desktop.git

    # then run
    cd openplc_desktop
    ./openplc-desktop.py

