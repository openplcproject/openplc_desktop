
import os

DOCS_PATH = os.path.join(os.path.dirname(__file__), "help_docs")

TEMPLATES_PATH = os.path.join(os.path.abspath(os.path.dirname(__file__)), "templates")
