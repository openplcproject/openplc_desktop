# -*- coding: utf-8 -*-

import os
import re
from configparser import ConfigParser
import collections

from Qt import QtCore, QtWidgets, QtGui, Qt, pyqtSignal


from img import Ico
import G
import ut

import cwidgets


class INIEditorWidget(QtWidgets.QWidget):

    def __init__(self, parent):
        QtWidgets.QWidget.__init__(self, parent)



        self.mainLayout = cwidgets.vlayout()
        self.setLayout(self.mainLayout)

        self.splitter = QtWidgets.QSplitter()
        self.mainLayout.addWidget(self.splitter)

        ## text edit
        self.txtEdit = QtWidgets.QPlainTextEdit()
        self.splitter.addWidget(self.txtEdit)

        f = self.txtEdit.font()
        f.setFamily("monospace")
        f.setPointSize(9)
        self.txtEdit.setFont(f)

        BasicIniHighlighter(self.txtEdit.document())


        ## tree
        self.tree = QtWidgets.QTreeWidget()
        self.splitter.addWidget(self.tree)
        self.tree.setRootIsDecorated(True)

        hitem = self.tree.headerItem()
        hitem.setText(0, "Key")
        hitem.setText(1, "Val")

        self.splitter.setStretchFactor(0, 2)
        self.splitter.setStretchFactor(1, 1)


    def init_load(self):

        pth = os.path.join(G.STATIC_PATH, "example.ini")
        s = ut.read_file(pth)
        self.txtEdit.setPlainText(s)

        parser = ConfigParser(dict_type=MultiOrderedDict)
        parser.read(pth)

        for section_name in parser.sections():
            pitem = cwidgets.XTreeWidgetItem()
            pitem.set(0, section_name, bold=True)
            self.tree.addTopLevelItem(pitem)

            #print('Section:', section_name)
            #print('  Options:', parser.options(section_name))
            for name, value in parser.items(section_name):
                item = cwidgets.XTreeWidgetItem(pitem)
                item.setText(0, name)
                item.setText(1, value)
                #print('  {} = {}'.format(name, value))
            #print()
            pitem.setExpanded(True)
        self.tree.resizeColumnToContents(0)
        self.tree.sortByColumn(0, Qt.AscendingOrder)


class MultiOrderedDict(collections.OrderedDict):
    def __setitem__(self, key, value):
        if isinstance(value, list) and key in self:
            self[key].extend(value)
        else:
            super(MultiOrderedDict, self).__setitem__(key, value)
            # super().__setitem__(key, value) in Python 3


class BasicIniHighlighter(QtGui.QSyntaxHighlighter):
    '''
    QSyntaxHighlighter class for use with QTextEdit for highlighting
    ini config files.

    I looked high and low to find a high lighter for basic ini config
    format, so I'm leaving this in the project even though I'm not
    using.
    '''

    def __init__(self, parent, theme=None):
        QtGui.QSyntaxHighlighter.__init__(self, parent)
        self.parent = parent

        self.highlightingRules = []

        # keyword
        self.highlightingRules.append(HighlightingRule(r"^[^:=\s][^:=]*[:=]",
                                                       Qt.blue,
                                                       Qt.SolidPattern))

        # section
        self.highlightingRules.append(HighlightingRule(r"^\[[^\]]+\]",
                                                       Qt.darkMagenta,
                                                       Qt.SolidPattern))

        # comment
        self.highlightingRules.append(HighlightingRule(r"#[^\n]*",
                                                       Qt.black,
                                                       Qt.SolidPattern))

        # comment
        self.highlightingRules.append(HighlightingRule(r";[^\n]*",
                                                       Qt.black,
                                                       Qt.SolidPattern))

    def highlightBlock(self, text):
        for rule in self.highlightingRules:
            for match in rule.pattern.finditer(text):
                self.setFormat(match.start(), match.end() - match.start(), rule.highlight)
        self.setCurrentBlockState(0)


class HighlightingRule():
    def __init__(self, pattern, color, style):
        if isinstance(pattern, str):
            self.pattern = re.compile(pattern)
        else:
            self.pattern = pattern
        charfmt = QtGui.QTextCharFormat()
        brush = QtGui.QBrush(color, style)
        charfmt.setForeground(brush)
        self.highlight = charfmt